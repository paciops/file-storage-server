# ROADMAP
 - [x] Implement correct Makefile
 - [x] Implement server ini file read
 - [x] Implement server threadpoll
 - [x] Implement basic client connection
 - [x] Implement api functions
 - [ ] Implement server functions
 - [x] Makefile test 3
 - [ ] Bash file statistic
 - [x] Implement client -h param parsing
 - [x] Implement client -p param parsing
 - [x] Implement client -f param parsing
 - [x] Implement client -t param parsing
 - [x] Implement client -W param parsing
 - [x] Implement client -w param parsing
 - [x] Implement client -r param parsing
 - [x] Implement client -R param parsing
 - [x] Implement client -l param parsing
 - [x] Implement client -u param parsing
 - [x] Implement client -c param parsing
 - [ ] Implement client -d param parsing
 - [ ] Implement client -D param parsing
 - [ ] Check every malloc return
 - [x] Check buffer dimension on object update 
 - [] Check buffer dimension on object append