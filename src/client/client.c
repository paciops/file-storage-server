#include <aio.h>
#include <dirent.h>
#include <getopt.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "libs/api/src/api.h"
#include "libs/utils/src/utils.h"

#define EXIT_SUCCESS 0
#define EXIT_FAILURE_ARG 2

/*
Params:
-h: stampa la lista di tutte le opzioni accettatedal client e termina immediatamente;

-f filename: specifica il nome del socket AF_UNIX a cui connettersi;

-w dirname[,n=0]: invia al server i file nella cartella 'dirname', ovvero
  effettua una richiesta di scrittura al server per i file. Se la directory
  'dirname' contiene altre directory,
  queste vengono visitate ricorsivamente fino a quando non si leggono 'n' file;
  se n=0 (o non è specificato) non c'è un limite superiore al numero di file da
  inviare al server (tuttavia non è detto che il server possa scriverli tutti).

-W file1[,file2]: lista di nomi di file da scrivere nel server separati da ',';ù

-D dirname: cartella in memoria secondaria dove vengono scritti (lato client) i file che il server rimuove a seguito di capacity misses (e che erano stati modificati da operazioni di scrittura) per servire le scritture richieste attraverso l’opzione ‘-w’ e ‘-W’. L’opzione ‘-D’ deve essere usata quindi congiuntamente all’opzione ‘-w’ o ‘-W’, altrimenti viene generato un errore. Se l’opzione ‘-D’ non viene specificata, tutti i file che il server invia verso il client a seguito di espulsioni dalla cache, vengono buttati via. Ad esempio, supponiamo i seguenti argomenti a linea di comando “-w send -D store”, e supponiamo che dentro la cartella ‘send’ ci sia solo i lfile ‘pippo’. Infine supponiamo che il server, per poter scrivere nello storage il file ‘pippo’ deve espellere il file‘pluto’ e ‘minni’. Allora, al termine dell’operazione di scrittura, la cartella ‘store’ conterrà sia il file ‘pluto’ che il file ‘minni’. Se l’opzione‘ -D ’non viene specificata, allora il server invia sempre i file ‘pluto’ e ‘minni’ al client, ma questi vengono buttati via;

-r file1[,file2]: lista di nomi di file da leggere dal server separati da ','
  (esempio: -r pippo,pluto,minni);

-R[n=0]: tale opzione permette di leggere 'n' file qualsiasi attualmente
  memorizzati nel server; se n=0 (o non è specificato) allora vengono letti
  tutti i file presenti nel server; 

-d dirname: cartella in memoria secondaria dove scrivere i file letti dal server 
  con l'opzione '-r' o '-R'. L'opzione -d va usata congiuntamente a '-r' o '-R', 
  altrimenti viene generato un errore; 
  Se si utilizzano le opzioni '-r' o '-R' senza specificare l'opzione '-d'i file 
  letti non vengono memorizzati sul disco;

-t time: tempo in millisecondi che intercorre tra l'invio di due richieste
  successive al server (se non specificata si suppone -t 0, cioè non c'è alcun
  ritardo tra l'invio di due richieste consecutive);

-l file1 [,file2]: lista di nomi di file su cui acquisirela mutua esclusione;

-u file1[,file2]: lista di nomi di file su cui rilasciarela mutua esclusione;

-c file1[,file2]: lista di file da rimuovere dal server se presenti;

-p: abilita le stampe sullo standard output per ogni operazione. Le stampe
  associate alle varie operazioni riportano almeno le seguenti informazioni:
  tipo di operazione, file di riferimento, esito e dove è rilevante i bytes
  letti o scritti.
*/

void usage(char* const name) {
    fprintf(stdout, "Usage: %s [-h help] [-t time] [-p printing] -f socket_name \n", name);
}

/* global var to check if printing is possbile */
int print_var = 0;

/*  Operations code start here  */
typedef enum Code_t {
    Write = 33,
    Read,
    Lock,
    Unlock,
    Delete = 99
} Code;

typedef struct Operation_t {
    Code code;                 // code of the operation
    const char* dest;          // string that could contain the path of a folder, according to code could be a
    const char* dirname;       // string that contain the path of a folder containing the files to read
    const char** files;        // list of files, could be NULL
    size_t files_count;        // contains number of files
    size_t n;                  // integer that contains the number of files to read/write, must be >= 0
                               // destination folder or a saving folder
    struct Operation_t* next;  // next operations, could be NULL
} Operation;

/*
  @brief Add op at the last element of head
  @param head head of the queue of operations
  @param op operations to insert into the queue
*/
void operation_add(Operation** head, Operation* op) {
    if (*head == NULL)
        *head = op;
    else {
        Operation* tail = *head;
        while (tail->next != NULL)
            tail = tail->next;
        tail->next = op;
    }
}

/*
  @brief Free memory occupied from the operations
  @param prev head of the queue of operations
*/
void operation_free(Operation* prev) {
    Operation* current = prev;
    while (current != NULL) {
        current = prev->next;
        free((char*)prev->dest);
        free(prev->files);
        free(prev);
        prev = current;
    }
    return;
}

/*  Operations code stop here  */

/*
  @brief Check if "n=x" is present in a string and return x, 0 otherwise
  @param str String containing the args 
*/
size_t check_n(const char* str) {
    if (str != NULL && str[0] == 'n' && str[1] == '=')
        return (size_t)strtoint(str + 2);
    else
        return 0;
}

/*
  @brief Extract a list of string comma separated from a string
  @param str String containing the list
  @param length Length of the list
  @return List of string
*/
const char** exctract_list(const char* str, size_t* length) {
    char* token;
    char* rest = (char*)str;
    const char** list = NULL;
    while ((token = strtok_r(rest, ",", &rest))) {
        (*length)++;
        // printf("Token: %s\t strlen: %ld\n", token, strlen(token));
        // printf("List length: %ld\n", *length);
        list = realloc(list, (*length) * sizeof(char*));
        list[(*length) - 1] = token;
    }
    return list;
}

/**
 * thanks to https://codeforwin.org/2018/03/c-program-to-list-all-files-in-a-directory-recursively.html
 * Lists all files and sub-directories recursively 
 * considering path as base path.
 */

/*
La scelta di passare una funziona è stat presa alle 23.20 del 27/10/2021
Non sarà la scelta migliore ma adesso non saprei più cosa inventarmi
*/
void listFilesRecursively(char* basePath, int count, void (*f)(const char*)) {
    if (count == 0) return;
    char* path;
    struct dirent* dp;
    DIR* dir = opendir(basePath);
    DIR* tmp;
    char** list = NULL;
    size_t list_size = 0;

    // Unable to open directory stream
    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL && count != 0) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
            size_t length = sizeof(basePath) + sizeof(dp->d_name) + 2;
            path = malloc(length * sizeof(char));
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);
            path[length - 1] = '\0';
            if (!(tmp = opendir(path))) {
                // file
                if (count != 0) count--;
                (*f)(path);
                free(path);
            } else {
                // folder
                list_size++;
                list = realloc(list, list_size * sizeof(char*));
                list[list_size - 1] = path;
            }
            closedir(tmp);
        }
    }
    for (size_t i = 0; i < list_size && count != 0; i++) {
        listFilesRecursively(list[i], count - 1, f);
        free(list[i]);
    }
    free(list);
    closedir(dir);
}
void print_output(const char* pStr, ...) {
    if (print_var) {
        va_list args;
        va_start(args, pStr);
        vprintf(pStr, args);
        va_end(args);
    }
}

void write_func(const char* pathname) {
    int err;
    ASSERT_NE(err, openFile(pathname, O_CREATE | O_LOCK), "Open file\t", -1);
    print_output("Open file:\t%s response: %d\n", pathname, err);
    ASSERT_NE(err, writeFile(pathname, NULL), "Write file\t", -1);
    print_output("Write file:\t%s response: %d\n", pathname, err);
    ASSERT_NE(err, unlockFile(pathname), "Unlock file\t", -1);
    print_output("Unlock file:\t%s response: %d\n", pathname, err);
}

int main(int argc, char* const* argv) {
    /* Params parsing */
    int opt, err, time = 0, msec = 10;
    const struct timespec abstime = {
        1,  // seconds
        0   // milliseconds
    };
    const char* socket_name = NULL;
    Operation* operations = NULL;

    // put ':' in the starting of the
    // string so that program can
    // distinguish between '?' and ':'
    while ((opt = getopt(argc, argv, ":f:t:w:W:r:R::l:u:c:ph")) != -1) {
        switch (opt) {
            case 'h':
                // optional but close process after calling it
                usage(argv[0]);
                operation_free(operations);
                return EXIT_SUCCESS;
            case 'p':
                // optiona
                print_var = 1;
                break;
            case 'f':
                // required
                socket_name = optarg;
                break;
            case 't':
                time = strtoint(optarg);
                break;
            case 'w': {
                // write n files contained in dirname, if n = 0 write all files contained
                Operation* op = malloc(sizeof(Operation));
                // extract dirname and n (if present)
                char* token;
                char* rest = optarg;
                op->code = Write;
                op->files = NULL;
                op->files_count = 0;
                op->dirname = strtok_r(rest, ",", &rest);
                // parsing n if present
                op->n = check_n(strtok_r(rest, ",", &rest));
                op->dest = NULL;
                op->next = NULL;
                operation_add(&operations, op);
                break;
            }
            case 'W': {
                // write list of files comma separated
                Operation* op = malloc(sizeof(Operation));
                op->code = Write;
                op->dirname = NULL;
                op->dest = NULL;
                op->files_count = 0;
                op->files = exctract_list(optarg, &op->files_count);
                op->n = 0;
                op->next = NULL;
                operation_add(&operations, op);
                break;
            }
            case 'R': {
                // reading n files from the server, if n=0 or missing then read all files
                Operation* op = malloc(sizeof(Operation));
                // extract dirname and n (if present)
                char* token;
                op->code = Read;
                op->files = NULL;
                op->files_count = 0;
                op->dirname = NULL;
                // parsing n if present
                if (optarg == NULL)
                    op->n = 0;
                else
                    op->n = check_n(strtok_r(optarg, ",", &optarg));
                op->dest = NULL;
                op->next = NULL;
                operation_add(&operations, op);
                break;
            }
            case 'r': {
                // read pathname of a list of files to read from the server
                Operation* op = malloc(sizeof(Operation));
                op->code = Read;
                op->dest = NULL;
                op->dirname = NULL;
                op->files_count = 0;
                op->files = exctract_list(optarg, &op->files_count);
                op->next = NULL;
                op->n = 0;
                operation_add(&operations, op);
                break;
            }
            case 'l': {
                // lock list of files
                Operation* op = malloc(sizeof(Operation));
                op->code = Lock;
                op->dest = NULL;
                op->dirname = NULL;
                op->files_count = 0;
                op->files = exctract_list(optarg, &op->files_count);
                op->next = NULL;
                op->n = 0;
                operation_add(&operations, op);
                break;
            }
            case 'u': {
                // unlock list of files
                Operation* op = malloc(sizeof(Operation));
                op->code = Unlock;
                op->dest = NULL;
                op->dirname = NULL;
                op->files_count = 0;
                op->files = exctract_list(optarg, &op->files_count);
                op->next = NULL;
                op->n = 0;
                operation_add(&operations, op);
                break;
            }
            case 'c': {
                // delete a list of flies
                Operation* op = malloc(sizeof(Operation));
                op->code = Delete;
                op->dest = NULL;
                op->dirname = NULL;
                op->files_count = 0;
                op->files = exctract_list(optarg, &op->files_count);
                op->next = NULL;
                op->n = 0;
                operation_add(&operations, op);
                break;
            }
            case ':':
                printf("Argument %s needs a value\n", argv[optind - 1]);
                break;
            case '?':
                printf("Unknown option: %c\n", optopt);
                break;
            default:
                printf("Default: %c %s\n", opt, optarg);
        }
    }
    if (argc == 1) {
        usage(argv[0]);
        return EXIT_FAILURE_ARG;
    }
    if (socket_name == NULL) {
        PRINT_ERROR("Missing argument -f socket_name\n");
        return EXIT_FAILURE_ARG;
    }

    // if operations is NULL dont open and close any connection
    if (operations != NULL) {
        // check every operation
        Operation* op_p = operations;
        ASSERT_NECE(err, openConnection(socket_name, msec, abstime), "Open connection", -1, operation_free, operations);
        while (op_p != NULL) {
            switch (op_p->code) {
                case Write: {
                    if (op_p->dirname != NULL) {
                        // -w
                        if (op_p->n > 0)
                            listFilesRecursively((char*)op_p->dirname, op_p->n, write_func);
                        else
                            listFilesRecursively((char*)op_p->dirname, -1, write_func);

                    } else {
                        // -W
                        for (size_t i = 0; i < op_p->files_count; i++) {
                            ASSERT_NE(err, openFile(op_p->files[i], O_CREATE | O_LOCK), "Open file\t", -1);
                            print_output("Open file:\t%s response: %d\n", op_p->files[i], err);
                            ASSERT_NE(err, writeFile(op_p->files[i], NULL), "Write file\t", -1);
                            print_output("Write file:\t%s response: %d\n", op_p->files[i], err);
                            ASSERT_NE(err, unlockFile(op_p->files[i]), "Unlock file\t", -1);
                            print_output("Unlock file:\t%s response: %d\n", op_p->files[i], err);
                        }
                    }
                    break;
                }
                case Read: {
                    if (op_p->files != NULL) {
                        // -r
                        for (size_t i = 0; i < op_p->files_count; i++) {
                            void* buffer;
                            size_t size = 0;
                            ASSERT_NE(err, readFile(op_p->files[i], &buffer, &size), "Read file\t", -1);
                            print_output("Read file:\t%s buffer size: %ld\tresponse: %d\n", op_p->files[i], size, err);
                            if (size > 0) free(buffer);
                        }

                    } else {
                        // -R
                        ASSERT_NE(err, readNFiles(op_p->n, NULL), "Read n files\t", -1);
                        print_output("Reading %d files:\tresponse: %d\n", op_p->n, err);
                    }
                    break;
                }
                case Lock: {
                    for (size_t i = 0; i < op_p->files_count; i++) {
                        ASSERT_NE(err, lockFile(op_p->files[i]), "Lock file\t", -1);
                        print_output("Lock file:\t%s response: %d\n", op_p->files[i], err);
                    }
                    break;
                }
                case Unlock: {
                    for (size_t i = 0; i < op_p->files_count; i++) {
                        ASSERT_NE(err, unlockFile(op_p->files[i]), "Unlock file\t", -1);
                        print_output("Unlock file:\t%s response: %d\n", op_p->files[i], err);
                    }
                    break;
                }
                case Delete: {
                    for (size_t i = 0; i < op_p->files_count; i++) {
                        ASSERT_NE(err, removeFile(op_p->files[i]), "Remove file\t", -1);
                        print_output("Remove file:\t%s response: %d\n", op_p->files[i], err);
                    }
                    break;
                }
            }
            op_p = op_p->next;
            usleep(time * 1000);
        }
        ASSERT_NECE(err, closeConnection(socket_name), "Close", -1, operation_free, operations);
    }

    operation_free(operations);
    return EXIT_SUCCESS;
}