#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
/**
 * Copyright (c) 2016 rxi
 */
#include <libs/ini/src/ini.h>
#include <libs/storage/src/storage.h>
#include <libs/utils/src/utils.h>

#define DEFAULT_CONFIG_FILE "configs/config.ini"
#define UNIX_PATH_MAX 108
#define DEBUG 1

#include <libs/signal_prj/src/signal_prj.h>
/* https://code-vault.net/lesson/w1h356t5vg:1610029047572 for thread pool
 * and task ideas
 */

/*  Task code */
typedef struct Task_t {
    int client_socket;
    OP operation;
    struct Task_t *next;
} Task;

pthread_mutex_t mutexTasks;
pthread_cond_t condQueue;

Task *head = NULL;
Task *tail = NULL;
int task_count = 0, task_count_max = 0;

void enqueue(Task *task) {
    pthread_mutex_lock(&mutexTasks);
    if (head == NULL) {
        head = task;
        tail = head;
    } else {
        tail->next = task;
        tail = tail->next;
    }
    task_count++;
    task_count_max = task_count_max < task_count ? task_count : task_count_max;
    if (DEBUG)
        printf("Task in queue: %d\t task max in queue: %d\n", task_count, task_count_max);
    pthread_mutex_unlock(&mutexTasks);
    pthread_cond_signal(&condQueue);
}

Task *dequeue() {
    pthread_mutex_lock(&mutexTasks);
    while (head == NULL) {
        switch (is_online()) {
            case RUN:
                pthread_cond_wait(&condQueue, &mutexTasks);
                break;
            case WAIT:
                //break;
            case CLOSE:
                pthread_mutex_unlock(&mutexTasks);
                return NULL;
        }
    }
    Task *task = head;
    head = head->next;
    task_count--;
    pthread_mutex_unlock(&mutexTasks);
    return task;
}

/*  Task code end */

void handle_connection(int client_socket) {
    Task *t = malloc(sizeof(Task));
    t->client_socket = client_socket;
    t->next = NULL;
    enqueue(t);
}

void *start_thread(void *arg) {
    int id = *((int *)arg), fd, err = 0, response;
    OP op;
    const char *pathname;
    size_t pathname_size = 0;

    while (1) {
        Task *task_p = dequeue();
        if (task_p == NULL)
            break;
        // read operation requested from client
        fd = task_p->client_socket;
        if (read_int(fd, (int *)&op) > 0) {
            int send = 1;
            if (DEBUG)
                printf("Thread #%d executing task of client %d  with operation %d\n", id, fd, op);

            // checking with operation is required
            switch (op) {
                case O_CREATE: {
                    // check if file exists, if not create a file with pathname,
                    // set buffer to null, set locked to -1 and append data later to it
                    // if it does not exists return -1
                    if (read_pathname(fd, &pathname, &pathname_size) == -1) {
                        printf("Error reading pathname\tClient: %d\t errno: %d\n", fd, errno);
                        err = 1;
                        free((char *)pathname);
                        break;
                    }
                    if (object_get(pathname, pathname_size) != NULL) {
                        response = -1;
                        free((char *)pathname);
                    } else {
                        response = 0;
                        object_add(pathname, -1);
                    }
                    break;
                }
                case O_LOCK: {
                    // check if file exists, if so responde with -1
                    // if not check if file is locked, if so responde with -2
                    // otherwise lock file and later write on it
                    if (read_pathname(fd, &pathname, &pathname_size) == -1) {
                        printf("Error reading pathname\tClient: %d\t errno: %d\n", fd, errno);
                        err = 1;
                        break;
                    }
                    switch (object_update_lock(pathname, fd, -1)) {
                        case 0: {
                            response = 0;
                            break;
                        }
                        case 1: {
                            response = -1;
                            break;
                        }
                        case 2: {
                            response = -2;
                            break;
                        }
                    }
                    free((char *)pathname);
                    break;
                }
                case O_UNLOCK: {
                    // check if the object is locked by this client,
                    // if so unlock it,
                    // otherwise responde with -2,
                    // if object does not exists response with -1
                    if (read_pathname(fd, &pathname, &pathname_size) == -1) {
                        printf("Error reading pathname\tClient: %d\t errno: %d\n", fd, errno);
                        err = 1;
                        break;
                    }
                    switch (object_update_lock(pathname, -1, fd)) {
                        case 0: {
                            response = 0;
                            break;
                        }
                        case 1: {
                            response = -2;
                            break;
                        }
                        case 2: {
                            response = -1;
                            break;
                        }
                    }
                    free((char *)pathname);
                    break;
                }
                case (O_CREATE | O_LOCK): {
                    // check if the file exists
                    // if not create it and lock it
                    // if exists check if it is locked
                    // if so than responde with -1
                    // otherwise lock it and responde with 0
                    if (read_pathname(fd, &pathname, &pathname_size) == -1) {
                        printf("Error reading pathname\tClient: %d\t errno: %d\n", fd, errno);
                        err = 1;
                        break;
                    }
                    Object *obj_p = object_get(pathname, pathname_size);
                    if (obj_p == NULL) {
                        response = 0;
                        // does not exist
                        object_add(pathname, fd);
                    } else {
                        switch (object_update_lock(pathname, fd, -1)) {
                            case 0: {
                                response = 0;
                                free((char *)pathname);
                                break;
                            }
                            case 1: {
                                response = -1;
                                free((char *)pathname);
                                break;
                            }
                            case 2: {
                                object_add(pathname, fd);
                                break;
                            }
                        }
                    }
                    break;
                }
                case O_CLOSE_CONNECTION: {
                    if (DEBUG)
                        printf("Thread #%d closing connection to client %d\n", id, fd);
                    response = 0;
                    break;
                }
                case O_WRITE: {
                    // check if object exists
                    // if not responde with -1
                    // otherwise check if it is locked by the client
                    // if not responde with -2
                    // if new buffer length is greater than the storage size max
                    // responde with -3 and object is left untouched
                    if (read_pathname(fd, &pathname, &pathname_size) == -1) {
                        printf("Error reading pathname\tClient: %d\t errno: %d\n", fd, errno);
                        err = 1;
                        break;
                    }
                    // read file
                    void *buffer;
                    size_t length = 0;
                    if (read_file(fd, &buffer, &length) == -1) {
                        printf("Error reading buffer\tClient: %d\t errno: %d\n", fd, errno);
                        err = 1;
                        free(buffer);
                        free((char *)pathname);
                        break;
                    }
                    // writing buffer to obj

                    Object *obj_p = object_get(pathname, pathname_size);
                    if (obj_p == NULL) {
                        // object is not in the storage
                        response = -1;
                        free((char *)pathname);
                        free(buffer);
                    } else {
                        // object is in the storage
                        // check if locked by client
                        if (obj_p->lockedBy != fd) {
                            // not locked by client
                            response = -2;
                            free((char *)pathname);
                            free(buffer);
                        } else {
                            response = 0;
                            // locked by user, can write on it
                            switch (object_update_buffer(pathname, buffer, length)) {
                                case 0:
                                    response = 0;
                                    break;
                                case 1:
                                    response = -3;
                                    free(buffer);
                                    break;
                                case 2:
                                    response = -1;
                                    free(buffer);
                                    break;
                            }
                            free((char *)pathname);
                        }
                    }
                    break;
                }
                case O_CLOSE: {
                    // TODO add a closed attribute to Object ?
                    // read pathname of object
                    if (read_pathname(fd, &pathname, &pathname_size) == -1) {
                        printf("Error reading pathname\tClient: %d\t errno: %d\n", fd, errno);
                        err = 1;
                        free((char *)pathname);
                        break;
                    }
                    if (DEBUG)
                        printf("Thread #%d closing file %s\n", id, pathname);
                    // set response
                    response = 0;
                    free((char *)pathname);
                    break;
                }
                case O_REMOVE: {
                    if (read_pathname(fd, &pathname, &pathname_size) == -1) {
                        printf("Error reading pathname\tClient: %d\t errno: %d\n", fd, errno);
                        err = 1;
                        free((char *)pathname);
                        break;
                    }
                    response = object_remove(pathname, fd) == 0 ? 0 : -1;
                    free((char *)pathname);
                    break;
                }
                case O_READ_N: {
                    int n = 0;
                    if (read_int(fd, &n) == -1) {
                        printf("Error reading number of files\tClient: %d\t errno: %d\n", fd, errno);
                        err = 1;
                        break;
                    }
                    int max = storage_file_count();
                    if (n == 0)
                        n = max;
                    else
                        n = max > n ? n : max;
                    if (write_int(fd, &n) == -1) {
                        printf("Error sending n to client %d\n", fd);
                        err = 1;
                        break;
                    }
                    for (int i = 0; i < n; i++) {
                        Object *obj_p = object_get_by_index(i);
                        // send pathname
                        if (write_pathname(fd, obj_p->pathname) == -1) {
                            printf("Error sending pathname to client %d\n", fd);
                            err = 1;
                            break;
                        }
                        // send buffer
                        if (write_buffer(fd, obj_p->buffer, obj_p->size) == -1) {
                            printf("Error sending buffer to client %d\n", fd);
                            err = 1;
                            break;
                        }
                    }
                    response = n;
                    break;
                }
                case O_READ: {
                    if (read_pathname(fd, &pathname, &pathname_size) == -1) {
                        printf("Error reading pathname\tClient: %d\t errno: %d\n", fd, errno);
                        err = 1;
                        free((char *)pathname);
                        break;
                    }
                    Object *obj_p = object_get(pathname, pathname_size);
                    if (obj_p == NULL) {
                        response = -1;
                    } else {
                        send = 0;
                        response = 0;
                        if (write_int(fd, &response) == -1) {
                            printf("Error sending confirm to client %d\n", fd);
                            err = 1;
                            response = -1;
                        }
                        if (write_buffer(fd, obj_p->buffer, obj_p->size) == -1) {
                            printf("Error sending buffer to client %d\n", fd);
                            err = 1;
                            break;
                        }
                    }
                    free((char *)pathname);
                    break;
                }
                case O_APPEND:
                default:
                    printf("Invalid operation\n");
                    response = -1;
                    break;
            }
            if (send && write_int(fd, &response) == -1) {
                printf("Error sending response to client %d\n", fd);
                err = 1;
            }
            // task is removed from the queue, pointer can bee freed
            // but client could have more to send (if connection is not close)
            if (op == O_CLOSE_CONNECTION || err) {
                ASSERT_NE(err, close(fd), "Close client socket", -1);
            } else
                handle_connection(fd);
        } else {
            printf("Cannot handle client %d, errno is %d\n", fd, errno);
            ASSERT_NE(err, close(fd), "Close client socket", -1);
        }
        free(task_p);
    }
    free(arg);
    return NULL;
}

int setup_server(struct sockaddr_un *psa) {
    int server_socket, err;
    ASSERT_NEE(server_socket, socket(AF_UNIX, SOCK_STREAM, 0), "Socket", -1);
    ASSERT_NEE(err, bind(server_socket, (struct sockaddr *)psa, sizeof(*psa)),
               "Bind", -1);
    ASSERT_NEE(err, listen(server_socket, SOMAXCONN), "Listen", -1);
    return server_socket;
}

void usage(const char *name) { printf("Usage: %s [INI CONFIG FILE]\n", name); }

int main(int argc, char const *argv[]) {
    const char *config_file_name = DEFAULT_CONFIG_FILE;
    int err;

    if (argc > 1) {
        config_file_name = argv[1];
    } else {
        printf("Using default config file: %s\n", config_file_name);
    }
    // Load config file
    ini_t *config = ini_load(config_file_name);
    setup_signal();

    // Get number of threads and init thread pool
    const char *str = ini_get(config, NULL, "n_threads");
    pthread_mutex_init(&mutexTasks, NULL);
    pthread_cond_init(&condQueue, NULL);
    int i, n_threads = strtoint(str);
    pthread_t threads[n_threads];
    str = ini_get(config, NULL, "n_files");
    int n = strtoint(str);
    str = ini_get(config, NULL, "space");
    int space = strtoint(str);
    storage_init(n, space);
    for (i = 0; i < n_threads; i++) {
        int *arg = malloc(sizeof(int));
        *arg = i;
        ASSERT_E(err, pthread_create(&threads[i], NULL, &start_thread, arg), "Failed to create the thread", 0);
    }
    str = ini_get(config, NULL, "socket_name");
    struct sockaddr_un sa;
    strncpy(sa.sun_path, str, UNIX_PATH_MAX);
    sa.sun_family = AF_UNIX;
    int server_socket = setup_server(&sa);
    ini_free(config);
    printf("Server is ready\n");
    while (1) {
        long connfd;
        ASSERT_NE(connfd, accept(server_socket, (struct sockaddr *)NULL, NULL),
                  "Accept", -1);
        if (connfd != -1) {
            if (is_online() == RUN) {
                handle_connection(connfd);
            } else {
                printf("But closing signal is sent...\n");
                break;
            }
        }
    }
    printf("Cleaning...\n");
    for (i = 0; i < n_threads; i++) {
        pthread_cond_broadcast(&condQueue);
        ASSERT_E(err, pthread_join(threads[i], NULL), "Failed to join thread", 0);
    }
    pthread_mutex_destroy(&mutexTasks);
    pthread_cond_destroy(&condQueue);
    storage_close();
    close_signal_handler();
    printf("Server is closing ...\nMax task count: %d\n", task_count_max);
    return 0;
}
