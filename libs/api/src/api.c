#include "api.h"

#include <errno.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#define __USE_XOPEN
#include <unistd.h>

#define UNIX_PATH_MAX 108

int usleep(useconds_t usec);
static int fd_skt;
static struct sockaddr_un sa; /* ind AF_UNIX */

/*
Send operation and pathname to client
Return response from client
*/
int sendOpAndPath(OP operation, const char* pathname) {
    int err = 0;
    if (pathname == NULL) {
        errno = EINVAL;
        return -1;
    }
    // Send op to server
    if (write_int(fd_skt, (int*)&operation) == -1) return -1;
    // Send pathname to server
    if (write_pathname(fd_skt, pathname) == -1) return -1;
    // Wait for response from server
    if (read_int(fd_skt, &err) == -1) return -1;
    return err;
}

/*
Concat dirname and pathname in a new string
*/
char* createPath(const char* dirname, const char* pathname) {
    size_t l = strlen(dirname) + strlen(pathname);
    char* res = malloc(l + 1);
    strcpy(res, dirname);
    strcat(res, pathname);
    res[l] = '\0';
    return res;
}

/*

*/
int receiveAndSaveFile(const char* dirname) {
    const char* filename;
    size_t filename_length, file_size;
    void* buffer;
    // Wait for pathname
    if (read_pathname(fd_skt, &filename, &filename_length) == -1) return -1;
    // Wait for file
    if (read_file(fd_skt, &buffer, &file_size) == -1) return -1;
    if (dirname != NULL) {
        // Create path
        const char* path = createPath(dirname, filename);
        // Save file
        FILE* file;
        if ((file = fopen(path, "wb")) == NULL) {
            errno = EINVAL;
            return -1;
        }
        int wrote = fwrite(buffer, 1, file_size, file);
        free((char*)path);
        fclose(file);
    }
    // Clean
    free(buffer);
    return 0;
}
/*
Viene aperta una connessione AF_UNIX al socket file sockname.
Se il server non accetta immediatamente la richiesta di connessione,
la connessione da parte del client viene ripetuta dopo 'msec'
millisecondi e fino allo scadere del tempo assoluto 'abstime'
specificato come terzo argomento.
Ritorna
  0 in caso di successo,
  -1 in caso di fallimento, errno viene settato opportunamente.
*/
int openConnection(const char* sockname, int msec,
                   const struct timespec abstime) {
    int time_tot = abstime.tv_sec * 1000 + abstime.tv_nsec / 1000000;
    if ((fd_skt = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) return -1;
    strncpy(sa.sun_path, sockname, UNIX_PATH_MAX);
    sa.sun_family = AF_UNIX;
    do {
        if (connect(fd_skt, (struct sockaddr*)&sa, sizeof(sa)) == 0) return 0;
        time_tot -= msec;
        /* L'implementazione dello "scorrimento" del tempo non è corretta, dovrei
     * contare quenti msec sono stati spesi da questa parte di codice e non
     * toogliere solo msec da time_tot*/
        usleep(msec * 1000);
    } while (time_tot != 0);
    errno = ETIME;
    return -1;
}

/*
Chiude la connessione AF_UNIX associata al socket file sockname.
Ritorna
  0 in caso di successo,
  -1 in caso di fallimento, errno viene settato opportunamente.
*/
int closeConnection(const char* sockname) {
    if (strncmp(sa.sun_path, sockname, (size_t)strlen + 1) != 0) {
        errno = EINVAL;
        return -1;
    } else {
        OP op = O_CLOSE_CONNECTION;
        int res;
        if (write_int(fd_skt, (int*)&op) == -1) return -1;
        if (read_int(fd_skt, &res) == -1) return -1;
        return close(fd_skt);
    }
}

/*
Richiesta di apertura o di creazione di un file.
La semantica della openFile dipende dai flags passati come
secondo argomento che possono essere O_CREATE ed O_LOCK.
  Se viene passato il flag O_CREATE ed il file esiste già
  memorizzato nel server,
  oppure il file non esiste ed il flag O_CREATE non è stato
  specificato, viene ritornato un errore.
In caso di successo, il file viene sempre aperto in lettura e scrittura,
ed in particolare le scritture possono avvenire solo in append.
Se viene passato il flag O_LOCK (eventualmente in OR con O_CREATE)
il file viene aperto e/o creato in modalità locked, che vuol dire
che l'unico che può leggere o scrivere il file 'pathname' è
il processo che lo ha aperto. Il flag O_LOCK può essere esplicitamente
resettato utilizzando la chiamata unlockFile, descritta di seguito.
Ritorna
  0 in caso di successo,
  -1 in caso di fallimento errno viene settato opportunamente.
*/
int openFile(const char* pathname, OP flags) {
    if (pathname == NULL) {
        errno = EINVAL;
        return -1;
    }
    int res = sendOpAndPath(flags, pathname);
    if (res == 0) return 0;
    errno = EBUSY;
    return -1;
}

/*
Legge tutto il contenuto del file dal server (se esiste) ritornando un
puntatore ad un'area allocata sullo heap nel parametro 'buf',
mentre 'size' conterrà la dimensione del buffer dati
(ossia la dimensione in bytes del file letto).
In caso di errore, 'buf' e 'size' non sono validi.
Ritorna
  0 in caso di successo,
  -1 in caso di fallimento, errno viene settato opportunamente.
*/
int readFile(const char* pathname, void** buf, size_t* size) {
    if (pathname == NULL) {
        errno = EINVAL;
        return -1;
    }
    int op = O_READ, err = 0;
    // Write op code
    if (write_int(fd_skt, &op) == -1) return -1;
    // Send file pathname
    if (write_pathname(fd_skt, pathname) == -1) return -1;
    // check if file is in the storage
    if (read_int(fd_skt, &err) == -1) return -1;
    if (err == -1) {
        errno = ENOENT;
        return -1;
    }
    // Read file
    printf("post read confirm\n");
    if (read_file(fd_skt, buf, size) == -1) return -1;
    return 0;
}

/*
Richiede al server la lettura di 'N' files qualsiasi
da memorizzare nella directory 'dirname' lato client.
Se il server ha meno di 'N' file disponibili, li invia tutti.
Se N <=0 la richiesta al server è quella di leggere tutti
i file memorizzati al suo interno.
Ritorna
  un valore maggiore o uguale a 0 in caso di successo
  (cioè ritorna il n. di file effettivamente letti),
  -1 in caso di fallimento, errno viene settato opportunamente.
*/
int readNFiles(int N, const char* dirname) {
    // Send operation to get list of files
    OP op = O_READ_N;
    size_t str_length = 0, buffer_size = 0;
    void* buffer;
    if (write_int(fd_skt, (int*)&op) == -1) return -1;
    if (write_int(fd_skt, &N) == -1) return -1;
    if (read_int(fd_skt, &N) == -1) return -1;
    const char** files_pathname = malloc(N * sizeof(char*));
    for (int i = 0; i < N; i++) {
        if (read_pathname(fd_skt, &files_pathname[i], &str_length) == -1) return -1;
        if (read_file(fd_skt, &buffer, &buffer_size) == -1) return -1;
        FILE* file;
        if (dirname != NULL) {
            const char* path = createPath(dirname, files_pathname[i]);
            if ((file = fopen(path, "wb")) == NULL) {
                errno = EINVAL;
                return -1;
            }
            int wrote = fwrite(buffer, 1, buffer_size, file);
            printf("Wrote file %s (%d bytes) in %s\n", files_pathname[i], wrote, dirname);
            free((char*)path);
            fclose(file);
        }
        free(buffer);
        free((char*)files_pathname[i]);
    }
    free((char**)files_pathname);
    if (read_int(fd_skt, &N) == -1) return -1;
    return N;
}

/*
Scrive tutto il file puntato da pathname nel file server.
Ritorna successo solo se la precedente operazione,
terminata con successo, è stata openFile(pathname,O_CREATE | O_LOCK).
Se 'dirname' è diverso da NULL, il file eventualmente spedito
dal server perchè espulso dalla cache per far
posto al file 'pathname' dovrà essere scritto in 'dirname';
Ritorna
  0 in caso di successo,
  -1 in caso di fallimento, errno viene settato opportunamente.
*/
int writeFile(const char* pathname, const char* dirname) {
    // connect to the server
    OP op = O_WRITE;
    int count = 0;
    void* buffer;

    if (pathname == NULL) {
        errno = EINVAL;
        return -1;
    }
    // Send operation
    if (write_int(fd_skt, (int*)&op) == -1) return -1;
    // Send pathname
    if (write_pathname(fd_skt, pathname) == -1) return -1;

    if (write_file(fd_skt, pathname, &buffer) == -1) return -1;
    free(buffer);
    if (read_int(fd_skt, &count) == -1) {
        return -1;
    }
    switch (count) {
        case -1:
            errno = EEXIST;
            return -1;
        case -2:
            errno = EBUSY;
            return -1;
        case -3:
            errno = EFBIG;
            return -1;
        default:
            // count will contain the number of file that the server will send
            while (count > 0) {
                if (receiveAndSaveFile(dirname) == -1) return -1;
                count--;
            }
            return 0;
    }
}

/*
Richiesta di scrivere in append al file 'pathname' i 'size' bytes
contenuti nel buffer 'buf'.
L'operazione di append nel file è garantita essere atomica dal
file server. Se 'dirname' è diverso da NULL, il file eventualmente
spedito dal server perchè espulso dalla cache per far posto ai
nuovi dati di 'pathname' dovrà essere scritto in 'dirname';
Ritorna
  0 in caso di successo,
  -1 in caso di fallimento, errno viene settato opportunamente.
*/
int appendToFile(const char* pathname, void* buf, size_t size,
                 const char* dirname) {
    if (pathname == NULL || buf == NULL || size == 0) {
        errno = EINVAL;
        return -1;
    }
    int op = O_APPEND, count = 0, res = 0;
    // Send operation to the server
    if (write_int(fd_skt, &op) == -1) return -1;
    // Send pathname
    if (write_pathname(fd_skt, pathname) == -1) return -1;
    // Wait for response
    // 1 if pathname exists
    // 0 if not
    if (read_int(fd_skt, &res) == -1) return -1;
    if (!res) {
        errno = ENOENT;
        return -1;
    }
    // Send buffer to append
    if (writen(fd_skt, buf, size) == -1) return -1;
    // Wait for count
    if (read_int(fd_skt, &count) == -1) {
        return -1;
    }
    // if count > 0 there are count files that
    // are sent to the client
    while (count > 0) {
        if (receiveAndSaveFile(dirname) == -1) return -1;
        count--;
    }

    return 0;
}

/*
In caso di successo setta il flag O_LOCK al file.
Se il file era stato aperto/creato con il flag O_LOCK e la
richiesta proviene dallo stesso processo, oppure se il file
non ha il flag O_LOCK settato, l’operazione termina
immediatamente con successo,
altrimenti l’operazione non viene completata fino a
quando il flag O_LOCK non viene resettato dal detentore della lock.
L’ordine di acquisizione della lock sul file non è specificato.
Ritorna
  0 in caso di successo,
  -1 in caso di fallimento, errno viene settato opportunamente.
*/
int lockFile(const char* pathname) {
    if (pathname == NULL) {
        errno = EINVAL;
        return -1;
    }
    switch (sendOpAndPath(O_LOCK, pathname)) {
        case 0:
            return 0;
        case -1:
            // file not found
            errno = ENOENT;
            return -1;
        case -2:
            errno = EBUSY;
            return -1;
        default:
            return -1;
    }
}

/*
Resetta il flag O_LOCK sul file ‘pathname’.
L’operazione ha successo solo se l’owner della lock è il
processo che ha richiesto l’operazione,
altrimenti l’operazione termina con errore.
Ritorna
  0 in caso di successo,
  -1 in caso di fallimento, errno viene settato opportunamente
*/
int unlockFile(const char* pathname) {
    if (pathname == NULL) {
        errno = EINVAL;
        return -1;
    }
    switch (sendOpAndPath(O_UNLOCK, pathname)) {
        case 0:
            return 0;
        case -1:
            // file not found
            errno = ENOENT;
            return -1;
        case -2:
            errno = EBUSY;
            return -1;
        default:
            return -1;
    }
}

/*
Richiesta di chiusura del file puntato da 'pathname'.
Eventuali operazioni sul file dopo la closeFile falliscono.
Ritorna
  0 in caso di successo,
  -1 in caso di fallimento, errno viene settato opportunamente.
*/
int closeFile(const char* pathname) {
    int res = sendOpAndPath(O_CLOSE, pathname);
    if (res != -1) return 0;
    return -1;
}

/*
Rimuove il file cancellandolo dal file storage server.
L'operazione fallisce se il file non è in stato locked, o è
in stato locked da parte di un processo client
diverso da chi effettua la removeFile.
*/
int removeFile(const char* pathname) {
    if (sendOpAndPath(O_REMOVE, pathname) == 0)
        return 0;
    else {
        errno = EBUSY;
        return -1;
    }
}
