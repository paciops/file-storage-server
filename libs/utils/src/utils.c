#include "utils.h"

#include <sys/stat.h>

int lstat(const char* restrict path, struct stat* restrict buf);

int strtoint(const char* str) {
    int err = 0, num = strtol(str, NULL, 10);
    /* check  EINVAL or ERANGE */
    ASSERT_NEE(err, errno, "strtol EINVAL", EINVAL);
    ASSERT_NEE(err, errno, "strtol ERANGE", ERANGE);
    return num;
}

int write_int(int fd, int* val) { return writen(fd, val, sizeof(int)); }
int read_int(int fd, int* val) { return readn(fd, val, sizeof(int)); }

int write_pathname(int fd, const char* pathname) {
    size_t l = strlen(pathname) + 1;
    if (write_int(fd, (int*)&l) == -1) return -1;
    return writen(fd, (void*)pathname, l * sizeof(char));
}
int read_pathname(int fd, const char** pathname, size_t* length) {
    if (read_int(fd, (int*)length) == -1) return -1;
    *pathname = malloc((*length) * sizeof(char));
    if (readn(fd, (void*)*pathname, *length) == -1) {
        free((char*)*pathname);
        return -1;
    }
    return 0;
}

int read_file(int fd, void** buffer, size_t* size) {
    // Read file size
    if (read_int(fd, (int*)size) == -1) return -1;
    *buffer = malloc(*size);
    // Read file content
    if (readn(fd, *buffer, *size) == -1) {
        free(*buffer);
        return -1;
    }
    return 0;
}

int write_buffer(int fd, void* buffer, size_t size) {
    if (write_int(fd, (int*)&size) == -1) return -1;
    return writen(fd, buffer, size);
}

int write_file(int fd, const char* pathname, void** buffer) {
    FILE* file;
    struct stat stats;
    if ((file = fopen(pathname, "rb")) == NULL || lstat(pathname, &stats) == -1) {
        errno = EINVAL;
        return -1;
    }
    // writing file size
    int size = (int)stats.st_size;
    *buffer = malloc(size);
    if (write_int(fd, &size) == -1) return -1;
    int read = fread(*buffer, 1, size, file), wrote;
    if ((wrote = writen(fd, *buffer, size)) == -1) return -1;
    //printf("Read=%d\tWrote=%d\n", read, wrote);
    fclose(file);
    return 0;
}