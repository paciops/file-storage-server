#ifndef UTIL_H
#define UTIL_H

#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Assert not equals*/
#define ASSERT_NE(var, f, msg, value) \
    if ((var = f) == value) {         \
        perror(msg);                  \
    }
/* Assert not equals and exit */
#define ASSERT_NEE(var, f, msg, value) \
    if ((var = f) == value) {          \
        perror(msg);                   \
        exit(errno);                   \
    }

/* Assert not equals, clean and exit */
#define ASSERT_NECE(var, f, msg, value, clean, ...) \
    if ((var = f) == value) {                       \
        clean(__VA_ARGS__);                         \
        perror(msg);                                \
        exit(errno);                                \
    }

/* Assert equals*/
#define ASSERT_E(var, f, msg, value) \
    if ((var = f) != value) {        \
        perror(msg);                 \
    }

/* Assert equals and exit */
#define ASSERT_EE(var, f, msg, value) \
    if ((var = f) != value) {         \
        perror(msg);                  \
        exit(errno);                  \
    }

/* Assert equals and exit */
#define ASSERT_ECE(var, f, msg, value, clean, ...) \
    if ((var = f) != value) {                      \
        clean(__VA_ARGS__);                        \
        perror(msg);                               \
        exit(errno);                               \
    }

#define PRINT_ERROR(msg) fprintf(stderr, msg);

/* thanks to https://stackoverflow.com/a/10041388 for enum idea */

typedef enum Op_t {
    O_CREATE = 1 << 0,  // binary 0001
    O_LOCK = 1 << 1,    // binary 0010
    O_READ = 1 << 2,    // binary 0100
    O_READ_N = 1 << 3,  // binary 1000
    O_WRITE = 1 << 4,
    O_APPEND = 1 << 5,
    O_UNLOCK = 1 << 6,
    O_CLOSE = 1 << 7,
    O_REMOVE = 1 << 8,
    O_CLOSE_CONNECTION = 1 << 9,
} OP;

int strtoint(const char* str);

/** Evita letture parziali
 *
 *   \retval -1   errore (errno settato)
 *   \retval  0   se durante la lettura da fd leggo EOF
 *   \retval size se termina con successo
 */
static inline int readn(long fd, void* buf, size_t size) {
    size_t left = size;
    int r;
    char* bufptr = (char*)buf;
    while (left > 0) {
        if ((r = read((int)fd, bufptr, left)) == -1) {
            if (errno == EINTR) continue;
            return -1;
        }
        if (r == 0) return 0;  // EOF
        left -= r;
        bufptr += r;
    }
    return size;
}

/** Evita scritture parziali
 *
 *   \retval -1   errore (errno settato)
 *   \retval  0   se durante la scrittura la write ritorna 0
 *   \retval  1   se la scrittura termina con successo
 */
static inline int writen(long fd, void* buf, size_t size) {
    size_t left = size;
    int r;
    char* bufptr = (char*)buf;
    while (left > 0) {
        if ((r = write((int)fd, bufptr, left)) == -1) {
            if (errno == EINTR) continue;
            return -1;
        }
        if (r == 0) return 0;
        left -= r;
        bufptr += r;
    }
    return 1;
}
int write_buffer(int fd, void* buffer, size_t size);

int write_int(int fd, int* val);
int read_int(int fd, int* val);

int write_pathname(int fd, const char* pathname);
int read_pathname(int fd, const char** pathname, size_t* length);

int read_file(int fd, void** buffer, size_t* size);
int write_file(int fd, const char* pathname, void** buffer);

#endif /* UTIL_H */