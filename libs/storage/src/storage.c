#include "storage.h"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef DEBUG
#define DEBUG 0
#endif
Object *storage;
pthread_mutex_t mutexObjects;
size_t files_count = 0, files_max, storage_size = 0, storage_size_max;

void _object_free(Object obj) {
    free(obj.buffer);
    free((char *)obj.pathname);
}

void _object_summary() {
    printf("\n#####################################################\n");
    printf("Actual storage size is %ld, full at %.2f %% and there are %ld files.\n", storage_size, 100 * (double)storage_size / (double)storage_size_max, files_count);
    size_t real = 0;
    for (size_t i = 0; i < files_count; i++) {
        printf("%ld) buffer size: %ld\tlocked by : %d\tname: %s\n", i, storage[i].size, storage[i].lockedBy, storage[i].pathname);
        real += storage[i].size;
    }
    printf("\n#####################################################\n");
    if (real != storage_size) {
        printf("\t\t\tERROR:\tReal size is %ld\n", real);
    }
}

Object *object_get(const char *pathname, size_t pathname_length) {
    Object *result = NULL;
    pthread_mutex_lock(&mutexObjects);
    for (size_t i = 0; i < files_count; i++) {
        if (strncmp(storage[i].pathname, pathname, pathname_length) == 0) {
            result = &storage[i];
            break;
        }
    }
    pthread_mutex_unlock(&mutexObjects);
    return result;
}

Object *object_get_by_index(size_t index) {
    Object *result = NULL;
    pthread_mutex_lock(&mutexObjects);
    if (index < files_count)
        result = &storage[index];
    pthread_mutex_unlock(&mutexObjects);
    return result;
}

uint8_t object_update_lock(const char *pathname, int value, int expected_val) {
    pthread_mutex_lock(&mutexObjects);
    // default result is not found, in this case 2
    uint8_t result = 2;
    for (size_t i = 0; i < files_count; i++) {
        if (strcmp(storage[i].pathname, pathname) == 0) {
            // if lockedBy is equal to the expected value
            // than update it
            // otherwise do nothing and return an error
            if (storage[i].lockedBy == expected_val) {
                storage[i].lockedBy = value;
                result = 0;
                break;
            } else {
                // locked by someone else
                result = 1;
                break;
            }
        }
    }
    _object_summary();
    pthread_mutex_unlock(&mutexObjects);
    return result;
}

uint8_t object_update_buffer(const char *pathname, void *new_buffer, size_t new_size) {
    if (new_size > storage_size_max) {
        if (DEBUG) printf("%s new buffer is %ld bytes and is bigger than the total storage size (%ld).\n", pathname, new_size, storage_size_max);
        return 1;
    }
    pthread_mutex_lock(&mutexObjects);
    // check if object is still in the storage
    Object *current;
    size_t free_space = 0;
    int index = -1;
    for (size_t i = 0; i < files_count && index == -1; i++) {
        if (strcmp(storage[i].pathname, pathname) == 0) {
            current = &storage[i];
            index = i;
            if (DEBUG) printf("Found %s at index %ld.\n", pathname, i);
        }
    }
    if (index == -1) {
        printf("%s is no more in the storage.\n", pathname);
        pthread_mutex_unlock(&mutexObjects);
        return 2;
    }
    free_space = storage_size_max - storage_size + current->size;
    if (new_size <= current->size || new_size <= free_space) {
        if (DEBUG) printf("Updating buffer of %s\n", pathname);
        // no need to remove other objects, there is enough space
        storage_size -= current->size;
        storage_size += new_size;
        free(current->buffer);
        current->buffer = new_buffer;
        current->size = new_size;
    } else {
        // need to remove some file in order to update the buffer
        if (DEBUG) printf("Freeing some space for %s, buffer size is %ld bytes.\n", pathname, new_size);
        // this map contains the number of the position that a object has to shift
        int *map = calloc(sizeof(int), files_count);
        for (size_t i = 0; i < files_count && storage_size_max - storage_size < new_size; i++) {
            map[i] = storage[i].size;
            if ((int)i != index && storage[i].size > 0) {
                // skip current object and objects that have size equals to zero
                storage_size -= storage[i].size;
                if (DEBUG) printf("Removing %s of length %ld, storage size is %ld\n", storage[i].pathname, storage[i].size, storage_size);
                free(storage[i].buffer);
                free((char *)storage[i].pathname);
                storage[i].pathname = NULL;
                storage[i].size = 0;
                map[i] = -1;
            }
        }
        storage_size -= current->size;
        storage_size += new_size;
        free(current->buffer);
        current->buffer = new_buffer;
        current->size = new_size;
        int negative_count = 0;
        printf("MAP:\n");
        for (size_t i = 0; i < files_count; i++) {
            if (map[i] == -1)
                negative_count++;
            else {
                map[i] = negative_count;
            }
            printf("%d | ", map[i]);
        }
        printf("\n");
        for (size_t i = 0; i < files_count; i++) {
            if (map[i] > 0) {
                storage[i - map[i]] = storage[i];
            }
        }
        files_count = files_count - map[files_count - 1];
        _object_summary();
        free(map);
    }
    _object_summary();
    pthread_mutex_unlock(&mutexObjects);
    return 0;
}

void object_add(const char *pathname, int lockedBy) {
    // when object are added the buffer is always empty
    pthread_mutex_lock(&mutexObjects);
    Object obj = {
        .pathname = pathname,
        .buffer = NULL,
        .size = 0,
        .lockedBy = lockedBy,
    };
    if (files_count < files_max) {
        // there is enough space
        storage[files_count] = obj;
        files_count++;
    } else {
        storage_size -= storage[0].size;
        if (DEBUG) printf("Removing %s at the top of the array\n", storage[0].pathname);
        _object_free(storage[0]);
        for (size_t i = 0; i < files_count - 1; i++)
            storage[i] = storage[i + 1];
        storage[files_count - 1] = obj;
    }
    if (DEBUG) printf("Added %s at the end of the array\n", obj.pathname);
    _object_summary();
    pthread_mutex_unlock(&mutexObjects);
}

uint8_t object_remove(const char *pathname, int client) {
    uint8_t result = 0, found = 0;
    pthread_mutex_lock(&mutexObjects);
    for (size_t i = 0; i < files_count && result == 0; i++) {
        printf("Scannig elements current: %s\n", storage[i].pathname);
        if (found) {
            storage[i - 1] = storage[i];
        } else if (strcmp(storage[i].pathname, pathname) == 0) {
            // check if it is locked by client
            printf("Element to delete found locked by %d\n", storage[i].lockedBy);
            if (client == storage[i].lockedBy) {
                // can remove it
                result = 0;
                found = 1;
                storage_size -= storage[i].size;
                _object_free(storage[i]);
            } else {
                // locked by someone else or not locked at all
                result = 1;
            }
        }
    }
    if (found) files_count--;
    _object_summary();
    pthread_mutex_unlock(&mutexObjects);
    return result;
}

size_t storage_file_count() {
    pthread_mutex_lock(&mutexObjects);
    size_t n = files_count;
    pthread_mutex_unlock(&mutexObjects);
    return n;
}

void storage_init(size_t f_max, size_t s_max) {
    if (f_max <= 0) {
        perror("Invalid max file number\n");
        exit(1);
    }
    if (s_max <= 0) {
        perror("Invalid file storage size\n");
        exit(1);
    }
    files_max = f_max;
    storage_size_max = s_max;
    pthread_mutex_init(&mutexObjects, NULL);
    storage = malloc(sizeof(Object) * files_max);
}

void storage_close() {
    pthread_mutex_destroy(&mutexObjects);
    if (DEBUG) printf("Storage cleaning\n");
    for (size_t i = 0; i < files_count; i++) {
        if (DEBUG) printf("Index: %ld\tpathname: %s\tbuffer length: %ld\n", i, storage[i].pathname, storage[i].size);
        _object_free(storage[i]);
    }
    free(storage);
}