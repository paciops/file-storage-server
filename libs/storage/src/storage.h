#ifndef STORAGE_H
#define STORAGE_H

#include <aio.h>
#include <stdint.h>

typedef struct Object_t {
    const char *pathname;
    void *buffer;
    size_t size;
    int lockedBy;
} Object;

Object *object_get(const char *, size_t);
/*
 @return NULL if index is out of bound, otherwise pointer at the index position
*/
Object *object_get_by_index(size_t index);

/*
 @brief Update lockedBy field of the object passed
 @param pathname Pathname of the bject to be modified
 @param value New value of the field lockedBy
 @param expected_val Value that we expect to find
 @return 0 if object is in the storage, expected value is equal to the current value of lockedBy and lockedBy is changed to value; 
 1 if lockedBy is not equal to expected_val;
 2 if the object is not  into the storage
*/
uint8_t object_update_lock(const char *pathname, int value, int expected_val);

/*
 @brief Update the buffer of an object
 @param pathname Pathname of the file to be edited
 @param new_buffer Buffer that will be updated
 @param new_size Size of the new buffer
 @return 0 if the buffer is updated correctly; 1 if new_size is greater than the total storage size; 2 if the file is not in the storage and another client has removed it.
*/
uint8_t object_update_buffer(const char *pathname, void *new_buffer, size_t new_size);
/*
 @brief Create a new object in the storage
 @param pathname Pathname of the object
 @param lockedBy Client that is locking the file
*/
void object_add(const char *, int);
/*
 @brief Remove on object from the storage if present and if locked
 @param pathanme Pathname of the object
 @param client Client that is trying to remove it
 @return 0 if file is removed correctly or not in the storage; 1 if file is not locked or locked by someone else
*/
uint8_t object_remove(const char *pathname, int client);
/*
 @return The number of files inside the storage
*/
size_t storage_file_count();

void storage_init(size_t, size_t);
void storage_close();

#endif /* STORAGE_H */