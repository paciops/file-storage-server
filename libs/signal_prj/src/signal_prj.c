#include "signal_prj.h"

#include "../../utils/src/utils.h"

volatile sig_atomic_t serveronline = RUN;
sigset_t mask;
pthread_t sighandler_thread;

int is_online() { return serveronline; }

void* sigHandler() {
  while (1) {
    int sig, err;
    ASSERT_EE(err, sigwait(&mask, &sig), "sigwait", 0);
    switch (sig) {
      case SIGINT: {
        char* msg = "\nSIGINT\n";
        write(1, msg, strlen(msg));
        serveronline = CLOSE;
        break;
      }
      case SIGHUP: {
        char* msg = "\nSIGHUP\n";
        write(1, msg, strlen(msg));
        serveronline = WAIT;
        break;
      }
      case SIGQUIT: {
        char* msg = "\nSIGQUIT\n";
        write(1, msg, strlen(msg));
        serveronline = CLOSE;
        break;
      }
      default:;
    }
    return NULL;
  }
  return NULL;
}

void setup_signal() {
  int err = 0;
  ASSERT_NE(err, sigemptyset(&mask), "sigemptyset", -1);
  ASSERT_NE(err, sigaddset(&mask, SIGINT), "sigaddset SIGINT", -1);
  ASSERT_NE(err, sigaddset(&mask, SIGHUP), "sigaddset SIGHUP", -1);
  ASSERT_NE(err, sigaddset(&mask, SIGQUIT), "sigaddset SIGQUIT", -1);
  ASSERT_EE(err, pthread_sigmask(SIG_BLOCK, &mask, NULL), "pthread_sigmask", 0);

  struct sigaction s;
  memset(&s, 0, sizeof(s));
  s.sa_handler = SIG_IGN;
  ASSERT_NEE(err, sigaction(SIGPIPE, &s, NULL), "sigaction", -1);

  ASSERT_EE(err, pthread_create(&sighandler_thread, NULL, sigHandler, NULL),
            "pthread_create signal", 0);
}

void close_signal_handler() { pthread_join(sighandler_thread, NULL); }