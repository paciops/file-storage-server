
#ifndef _POSIX_C_SOURCE
#define _XOPEN_SOURCE 700
#endif

#ifndef SIGNAL_H
#define SIGNAL_H
#include <pthread.h>
#include <signal.h>
#include <stddef.h>
#include <string.h>

#define RUN 1    // Keep going
#define WAIT 2   // Wait client to close
#define CLOSE 3  // Close without waiting

int is_online();
void setup_signal();
void close_signal_handler();

#endif