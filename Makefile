CC			= gcc
WARNINGS	= -g  -Wall -Wextra -Wno-unused-variable -pedantic -D_POSIX_C_SOURCE=200112L
CFLAGS  	= -std=c99 $(WARNINGS) -lpthread
AR			= ar
ARFLAGS		= rvs
LIB			= libs
OBJ			= obj
SRC			= src
ARCHIVE		= riccardo_pacioni-CorsoA
SERVERLIB	= utils ini signal_prj storage
CLIENTLIB	= utils api
EXE			= out
CONF		= configs
TARGETS		= c server client

all: $(TARGETS)

client: libclient
	$(CC) $(CFLAGS) $(SRC)/$@/$@.c -o $@.$(EXE) $(PWD)/$(OBJ)/$<.a -I .

server: libserver
	$(CC) $(CFLAGS) $(SRC)/$@/$@.c -o $@.$(EXE) $(PWD)/$(OBJ)/$<.a -I .

libserver: $(SERVERLIB)
	$(AR) $(ARFLAGS) $(OBJ)/$@.a $(foreach t, $^, $(OBJ)/$(t).o)

libclient: $(CLIENTLIB)
	$(AR) $(ARFLAGS) $(OBJ)/$@.a $(foreach t, $^, $(OBJ)/$(t).o)

%:
	@mkdir -p $(OBJ)
	$(CC) $(CFLAGS) -c -o $(OBJ)/$@.o $(LIB)/$@/$(SRC)/$@.c 

archive: clean
	tar -czf $(ARCHIVE).tar.gz --exclude ".git" .

test1: all
	@echo Starting test $@
	valgrind --leak-check=full ./server.$(EXE) $(CONF)/$@.ini & echo "$$!" > "server.pid"
	./scripts/test1.sh "test_one.sk"
	sleep 10
	cat server.pid | xargs kill -1
	@echo $@ executed correctly

test2: all
	@echo Starting $@
	./server.$(EXE) $(CONF)/$@.ini & echo "$$!" > "server.pid"
	./scripts/test2.sh "test_two.sk"
	sleep 10
	cat server.pid | xargs kill -1
	rm server.pid
	@echo $@ executed correctly

test3: all
	@echo Starting $@
	./server.$(EXE) $(CONF)/$@.ini & echo "$$!" > "server.pid"
	./scripts/test3.sh "test_three.sk" &
	sleep 3
	cat server.pid | xargs kill -2
	killall test3.sh
	-killall client.out
	rm server.pid
	@echo $@ executed correctly

generate_input_files: clean_input_files
	@mkdir -p input_files
	./scripts/generate_random_json.sh
	wget -P input_files http://www.penn.museum/collections/assets/data/american-csv-latest.zip
	unzip input_files/american-csv-latest.zip -d input_files
	-rm input_files/american-csv-latest.zip

clean_input_files:
	-rm input_files/ -rf

clean:
	@echo Cleaning...
	-rm -rf $(OBJ) *.out $(ARCHIVE).tar.gz vgcore.* valgrind_output.txt* *.sk *.pid
	@echo Done!

c: clean

.PHONY: c clean test1 test2 all archive generate_input_files clean_input_files