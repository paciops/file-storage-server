#!/bin/bash

folder=input_files/

socket=${1:-test.sk}

files=$(find $folder -type f | head -5 )
batch_1=$(echo $files | sed 's/ /,/g')

files=$(find $folder -type f | head -n 15 | tail -5)
batch_2=$(echo $files | sed 's/ /,/g')

files=$(find $folder -type f | tail -5 )
batch_3=$(echo $files | sed 's/ /,/g')

for i in {1..3}; do
    ./client.out -pf $socket -W $batch_1 -t 0 &
    ./client.out -pf $socket -W $batch_2 -t 0 &
    ./client.out -pf $socket -W $batch_3 -t 0 &
done
# TODO use while loop
# while true; do

# done