#!/bin/bash

socket=${1:-test.sk}
for i in {1..200}; do
    for filename in input_files/*.json; do
        ./client.out -pf $socket -W $filename &
    done
done
