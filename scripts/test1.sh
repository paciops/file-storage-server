#!/bin/bash

socket=${1:-test.sk}
for filename in input_files/*.json; do
    ./client.out -pf $socket -t 200 -w input_files/ -W $filename -R -r input_files/ -l $filename -u $filename -c $filename &
done